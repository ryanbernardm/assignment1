import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class OpenStackMeetingsServlet extends HttpServlet {


	private static final long serialVersionUID = 1L;
	ArrayList<String> history;
	ArrayList<String> data;
	PrintWriter w;

	@Override
	public void init(ServletConfig config) {
		history = new ArrayList<String>();
		data = new ArrayList<String>();
	}


	//handles GET requests, prints out history and data after querying
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		w = response.getWriter();
		String session_value = request.getParameter("session");
		HttpSession session = request.getSession(false);
		String project = request.getParameter("project");
		String year = request.getParameter("year");
		Document document = Jsoup.connect("http://eavesdrop.openstack.org/meetings").get();
		if(session_value != null && (session_value.equalsIgnoreCase("start") || session_value.equalsIgnoreCase("end"))){
				history = new ArrayList<String>();
				data = new ArrayList<String>();
			if (session_value.equalsIgnoreCase("start")) {
				session = request.getSession(true);
				w.println("History<br>");
				w.println("<br> Data");
			}else if (session_value.equalsIgnoreCase("end")) {
				printHistory();
				w.println("<br><br> Data");
				session.invalidate();
			}
			return;
		}	
		if (project == null || year == null) {
			if (project == null) {
				w.println("Required parameter &lt;project&gt; missing<br>");
			}else if (year == null) {
				w.println("Required parameter &lt;year&gt; missing<br>");
			}
			if(session == null){
				data = new ArrayList<String>();
				history = new ArrayList<String>();
			}else if(!session.isNew()){
				data = new ArrayList<String>();
			}
			return;
		}
		if (session == null) {
			data = new ArrayList<String>();
			history = new ArrayList<String>();
			if (query(w, request, project, year, document)) {
				w.println("History");
				printData();
			}
			data = new ArrayList<String>();
		} else if (!session.isNew()) {
			data = new ArrayList<String>();
			if (query(w, request, project, year, document)) {
				printHistory();
				printData();
			}
		} else {
			if (query(w, request, project, year, document)) {
				printHistory();
				printData();
			}
		}
		return;
	}
	
	//helper method to print history, utilized in doGet
	private void printHistory(){
		w.println("History");
		int index = 0;
		String url;
		while(index < history.size()){
			url = history.get(index);
			w.println("<br>" + url);
			index++;
		}
	}
	
	//helper method to print data, utilized in doGet
	private void printData(){
		w.println("<br><br> Data");
		int index = 0;
		String datum;
		while(index < data.size()){
			datum = data.get(index);
			w.println("<br>" + datum);
			index++;
		}
	}

	//query the website, returns true if successful and false otherwise.
	private boolean query(PrintWriter w, HttpServletRequest request, String project, String year, Document document)
			throws IOException {
		
		Elements links = document.getElementsByAttributeValue("href", project + "/");
		boolean flag = false;
		if(links == null || links.isEmpty()){
			w.println("Project with &lt;" + project + "&gt; not found<br>");
		}else{
			document = Jsoup.connect(links.attr("abs:href")).get();
			links = document.getElementsByAttributeValue("href", year + "/");
			if(links == null || links.isEmpty()){
				w.println("Invalid year &lt;" + year + "&gt; for project " + project + "<br>");
			}else{
				document = Jsoup.connect(links.attr("abs:href")).get();
				links = document.getElementsByAttributeValueStarting("href", project);
				history.add(request.getRequestURL().toString()+ (request.getQueryString() != null ? "?" 
								+ request.getQueryString().toString() : ""));
				if(links != null && !links.isEmpty()){
					for (Element link : links) {
						data.add(link.html());
					}
				}
			flag = true;
			}
		}
		return flag;
	}
}